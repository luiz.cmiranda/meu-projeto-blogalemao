import axios from "../axios/axios";

const pegarPostsPeloIdCategoria = async ( id) => {

    try {
        const resposta = await axios.get(`post/categoria/${id}`)
        return resposta.data.detalhes
    } catch(e) {
        console.log('Ocorreu um erro');
    }
}

export default pegarPostsPeloIdCategoria
