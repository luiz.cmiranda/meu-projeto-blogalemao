import axios from "../axios/axios";

const removerPostServico = async (idPost) => {

   

try {
const resposta = await axios.delete(`post/${idPost}`)

return {
    "sucesso": true,
    "mensagem": 'O post foi removido'
}

}
catch(erro) {
    return {
        "sucesso": false,
        "mensagem": 'Ocorreu um erro'
}


}

}
export default removerPostServico