import axios from "../axios/axios";


const pegarCategorias = async saveState => {

    // aqui tem um Mock
//     const _listaCategorias = [
//     {  
//         'id':1,
//         'descricao':'Verbos' 
//     },

//     {  
//         'id':2,
//         'descricao':'Números' 
//     },

//     {  
//         'id':3,
//         'descricao':'Formação de Frases' 
//     },

//     {  
//         'id':4,
//         'descricao':'W-Frage' 
//     }
  

//   ]

try {
    const resposta  = await  axios.get('categoria')
    const _listaCategorias = resposta.data
  
  
   _listaCategorias.sort( (a, b) => {
      return (a.descricao > b.descricao) ? 1 : -1;
  } )

  
  saveState(_listaCategorias) 
} catch(erro) {
    console.log(`ocerreu um erro: ${erro.message}.`)
  }
  
}  

export default pegarCategorias
    