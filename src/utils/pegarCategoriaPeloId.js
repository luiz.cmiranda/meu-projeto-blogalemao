import axios from "../axios/axios";

const pegarCategoriaPeloId = async (id) => {

    try{
    const resposta = await axios.get(`categoria/${id}`)
    return resposta.data.detalhes
}catch (e) {
    console.log('Houve um erro');
}

}
export default pegarCategoriaPeloId