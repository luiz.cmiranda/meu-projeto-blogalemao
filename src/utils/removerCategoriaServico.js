import axios from "../axios/axios";

const removerCategoriaServico = async (id) => {

  try{
       const resposta = await axios.delete(`categoria/${id}`)
       
       return {
        "sucesso": true,
        "mensagem": 'A categoria foi removida'
    }
    
    }
    catch(erro) {
        return {
            "sucesso": false,
            "mensagem": 'Ocorreu um erro'
    }

 }
} 

export default removerCategoriaServico