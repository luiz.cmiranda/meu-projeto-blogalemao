import axios from "../axios/axios";


const salvarNovoPostApi = async (novoPost, setMensagem) => {
  try {
     const resposta = await  axios.post('post', novoPost)

     setMensagem('Foi cadastrado um novo post')
  }
  catch(erro) {
     setMensagem( 'Ocorreu um erro ão cadastrar um novo post' )
  }
}

export default salvarNovoPostApi