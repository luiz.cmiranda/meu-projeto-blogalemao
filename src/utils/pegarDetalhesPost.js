import axios from "../axios/axios";

const pegarDetalhesPost = async (id) => {

try {
    const resposta = await axios.get(`post/${id}`)
    console.log(resposta);

    return resposta.data.detalhes
} catch(e) {
     alert('Houve algum erro...')
}

}

export default pegarDetalhesPost