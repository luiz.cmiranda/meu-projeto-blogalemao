import React, {useContext} from 'react';

import './Busca.css';

import TemaContex from '../../../contexts/TemaContext';

const Busca = () => {

    const tema = useContext(TemaContex);
    
    return(
     <div style={{backgroundColor: tema.corFundoTema}} id='busca'   >
      
     <div id='text-img'>
     <img  src='/imagens/leo.png'/> <a className='link' href='https://dict.leo.org/alem%C3%A3o-portugu%C3%AAs/'>
         Dicionário LEO </a> 

     </div> 


     </div>   
    )
} 

export default Busca;