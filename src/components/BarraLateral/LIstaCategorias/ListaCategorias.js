import React, {useContext} from 'react';
import {Link} from 'react-router-dom';

import './ListaCategorias.css';

import TemaContex from '../../../contexts/TemaContext';

const ListaCategorias = ( {lista}) => {

    const tema = useContext(TemaContex);

    return(
     <div style={{backgroundColor: tema.corFundoTema}} id='bl-lista-categorias'>
         

       <h4>Categorias:</h4>  
       
       
        <ul>
            {lista.map( item => {
                return <li key={item.id}>
                    <Link to={`/posts-por-categoria/${item.id}`}>
                    {item.descricao}
                    </Link>
                    </li>;
            })}
          
        </ul>
           
     </div>   
    )
} 

export default ListaCategorias;