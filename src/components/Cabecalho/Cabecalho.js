import React, {Link}  from 'react-router-dom';
import './Cabecalho.css';
import MenuPrincipal from './MenuPrincipal/MenuPrincipal';


const Cabecalho = ({functionConfigureTema}) => {
    return(
     <header>

<div id='titulo-cabecalho-temas'>

              
             <h1 className='titulo'> 
             <Link to= '/lista-posts' className= "titulo-top">Descobrindo o Alemão </Link>
             </h1>  
             
            
              
  

               <div id='c-temas'>

               <strong><p>Temas:</p></strong>

              

               <button id='cabecalho-tema-azul'
                onClick={ () => { functionConfigureTema('azul')}}
                >Azul</button>
               

               <button id='cabecalho-tema-verde'
                onClick={ () => { functionConfigureTema('verde')}}
                >Verde</button>
              

               <button id='cabecalho-tema-rosa'
                onClick={ () => { functionConfigureTema('rosa')}}
                >Rosa</button>

               <button id='cabecalho-tema-roxo'
               onClick={ () => { functionConfigureTema('roxo')}}
               >Roxo</button>

             
           


               </div>
       </div> 

       <strong><MenuPrincipal/></strong>

             

     </header> 
     
     
     
    )
} 

export default Cabecalho;