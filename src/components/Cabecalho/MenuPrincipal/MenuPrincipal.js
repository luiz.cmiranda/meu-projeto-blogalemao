import React,  {useContext} from 'react';

import {Link} from 'react-router-dom';

import './MenuPrincipal.css';

import TemaContex from '../../../contexts/TemaContext';

const MenuPrincipal =() => {

     const tema = useContext(TemaContex);

    return(
     <nav style={{backgroundColor: tema.corFundoTema}}>
      
            <ul>
                <li>
                    <Link to='/lista-posts'>Post</Link>
                </li>    
                <li> 
                     <Link to='/lista-categorias'>Categorias</Link>
                </li>    
                <li>
                     <Link to='/novo-post'>Novo Post</Link>
                </li>    

                <li>
                     <Link to='/Contato'>Contato</Link>
                </li>    

                <li>
                     <Link to='/Sobre'>Sobre</Link>
                </li>   


            </ul> 

            

     </nav>   
    )
} 

export default MenuPrincipal;