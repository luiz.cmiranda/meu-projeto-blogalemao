import React, {useContext} from 'react';

import './Rodape.css';

import TemaContex from '../../contexts/TemaContext';

const Rodape = () => {

  const tema = useContext(TemaContex);
  
    return(
     <footer style={{backgroundColor: tema.corFundoTema}} >
    
    
       <strong> Copyright &#169;<a  id="link" href="https://www.linkedin.com/in/luiz-carlos-azevedo-miranda-64829a211/"> 
       Luiz Azevedo </a> </strong>
       

     </footer>   
    )
} 

export default Rodape;