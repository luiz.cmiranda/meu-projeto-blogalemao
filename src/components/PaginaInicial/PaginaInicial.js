import React, { useState,  } from 'react';
import {BrowserRouter} from 'react-router-dom';


import './PaginaInicial.css';

import Cabecalho from '../Cabecalho/Cabecalho';
import ConteudoCentral from '../ConteudoCentral/ConteudoCentral';
import BarraLateral from '../BarraLateral/BarraLateral';
import Rodape from '../Rodape/Rodape';
import TemaContex from '../../contexts/TemaContext';





const PaginaInicial = () => {

 
    const [tema, setTema] = useState('roxo');

    const modificarTema = temaSelecionado => {
    
     switch(temaSelecionado) {
        
         case 'azul':
            const tema1 = {
                corFundoTema:'#5C9AFF',
                corFundoBotaoContinueLendo:'blue',
                corTextoBotaoContinueLendo: 'white'
             }
             setTema(tema1)
         break;
        
         case 'verde':
            const tema2 = {
                corFundoTema:'#49D4B9',
                corFundoBotaoContinueLendo:'green',
                corTextoBotaoContinueLendo: 'white'
             }
             setTema(tema2)  
         break;
         
         case 'rosa':
            const tema3 = {
                corFundoTema:'#FF70AE',
                corFundoBotaoContinueLendo:'pink',
                corTextoBotaoContinueLendo: 'black'
             }
             setTema(tema3)
         break;

         default: 
            const tema4 = {
                corFundoTema:'#931FFF',
                corFundoBotaoContinueLendo:'#7400E0',
                corTextoBotaoContinueLendo: 'white'
             }
             setTema(tema4)
             break;

            } 

    
    }

    return(
<TemaContex.Provider  value={tema}>
        <BrowserRouter  >
                <div id='box-pagina-inicial'>

                    <Cabecalho functionConfigureTema={modificarTema} />

                    

                <div id='container'>
                    <ConteudoCentral />
                    <BarraLateral />
                    
                </div>

                
                    <Rodape    />
                    
                    
            </div>
        </BrowserRouter>
 </TemaContex.Provider>        
    )
} 

export default PaginaInicial;