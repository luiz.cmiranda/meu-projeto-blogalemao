
import React, { useEffect, useState } from 'react';
import {useParams} from 'react-router-dom';

import './DetalhesPost.css';



import pegarDetalhesPost from '../../../utils/pegarDetalhesPost';
import pegarCategoriaPeloId from '../../../utils/pegarCategoriaPeloId';





const DetalhesPost =() => {

 const {id} = useParams();

 const [postCarregado, setPostCarregado] = useState({})

 const [nomeCategoria, setNomeCategoria] = useState('')


 useEffect(async () => {
    const _postCarregado = await pegarDetalhesPost(id)
    setPostCarregado(_postCarregado)

    const categoria = await pegarCategoriaPeloId(_postCarregado.categoriaId)
    setNomeCategoria(categoria.descricao)
 }, [] )


    return(
        
     <div id='cc-detalhes-post' >

         <h2>#{postCarregado.id} - {postCarregado.titulo}</h2> 

         <h4>{nomeCategoria} - {postCarregado.dataPostagem} - {postCarregado.autor} </h4>

         <img id="carregar-img" src={postCarregado.imagem} />

    
        
         <div id="ccdp-descricao"> {postCarregado.descricao}</div> 
         

       
     </div>
    )
} 

export default DetalhesPost;