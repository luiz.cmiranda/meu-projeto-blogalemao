import React, {useContext, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

import './Post.css';

import TemaContex from '../../../../contexts/TemaContext';
import removerPostServico from '../../../../utils/removerPostServico';
import pegarCategoriaPeloId from '../../../../utils/pegarCategoriaPeloId';

const Post = ( {post}) => {

    const tema = useContext(TemaContex);

    const [postRemovido, setPostRemovido] = useState(false)
    const [nomeCategoria, setNomeCategoria] = useState('')


    useEffect( async () => {
      const categoria = await pegarCategoriaPeloId(post.categoriaId)
      setNomeCategoria(categoria.descricao)
    }, [])

    const removerPost = async () => {
     const resultado = await removerPostServico(post.id)

      if(resultado.sucesso) {
          alert(resultado.mensagem)
          setPostRemovido(true)
          return false
      }
        alert(resultado.mensagem)

    }

    const tag = post.dataPostagem.substring(8, 10)
    const monate =  post.dataPostagem.substring(5,7)
    const yare = post.dataPostagem.substring(0, 4)

    const neuData = `${tag}/${monate}/${yare}`
    

    return(
      <>
      {postRemovido ? null: 

     <article className='post' style={{backgroundColor: tema.corFundoTema}}>
         
         <div className='titulo-botao'>
                <h3 className='p-titulo'>{post.titulo}</h3>
                
                <button className='lc-item-btn-remov'
                onClick={ () => removerPost()} >   
                Remover
                </button> 
         </div>

                <p className='p-postado-em'>Postado em: {neuData}</p>

         <div className='p-img-texto'>

           
                 <img className='p-img' alt='pp-img' src={post.imagem}/>  
                 
                    
                    
                    <div className='p-texto'>
                    {post.descricao}
                    </div>
                    

                    
         
         </div>
                <div className='p-categoria-botao'>

                <div className='p-categoria'>
                   <strong>Categoria: {nomeCategoria}</strong> 

                </div>   

                <Link 
                to={`/detalhes-post/${post.id}`}
                className='p-botao-continue-lendo'
                style={{backgroundColor: tema.corFundoBotaoContinueLendo, color: tema.corTextoBotaoContinueLendo}}
                > Continue Lendo...
                </Link>
          
                </div>
    </article>
      }
    </>
    )
} 

export default Post;