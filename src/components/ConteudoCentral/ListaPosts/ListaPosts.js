

import React from 'react';

import './ListaPosts.css';

import Post from './Post/Post';


const ListaPosts =({lista}) => {
    return(
        
        
        <div className='posts-todos'>
         {lista.length > 0 ?
         lista.map(post => <Post post={post}/> )
         : <p>carregando...</p>
         }
       </div>
    )
} 

export default ListaPosts;

