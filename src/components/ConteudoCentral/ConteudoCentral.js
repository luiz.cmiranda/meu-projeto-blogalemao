import React, {useState, useEffect} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import './ConteudoCentral.css';

import ListaPosts from './ListaPosts/ListaPosts';
import ListaCategorias from './ListaCategorias/ListaCategorias'
import NovoPost from './NovoPost/NovoPost';
import Contato from './Contato/Contato';
import Sobre from './Sobre/Sobre';
import DetalhesPost from './DetalhesPost/DetalhesPost';
import PostsPorCategoria from './PostsPorCategoria/PostsPorCategoria';
import NovaCategoria from './NovaCategoria/NovaCategoria'

import pegarCategorias from '../../utils/pegarCategorias';
import pegarPosts from '../../utils/pegarPosts';


const ConteudoCentral = () => {

  const [categorias, setCategorias] = useState([]);
  const [posts, setPosts] = useState([]);

  useEffect( () => {
      pegarCategorias( setCategorias)
  },[] );

  useEffect( () => {
    pegarPosts( setPosts)
},[] );

    return(
     <main>

       <Switch>

         <Route exact path="/"><Redirect to="/lista-posts"/></Route>

         <Route path="/lista-posts"><ListaPosts lista={posts} /> </Route> 

         <Route path="/lista-categorias"><ListaCategorias lista={categorias} /> </Route> 

         <Route path="/novo-post"><NovoPost categorias={categorias} /> </Route> 

         <Route path="/Contato"><Contato  /></Route>

         <Route path="/Sobre"><Sobre /></Route>
          
          {/* outra forma de escrita  */}
         <Route path="/Detalhes-Post/:id" component={DetalhesPost}/> 

         <Route path="/posts-por-categoria/:idCategoria" component={PostsPorCategoria}/> 

         <Route path="/nova-categoria" component={NovaCategoria}/> 

         
       </Switch>

     </main>   
    )
} 

export default ConteudoCentral;