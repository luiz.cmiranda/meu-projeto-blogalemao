import React, { useState } from 'react';

import './NovoPost.css';

import salvarNovoPostApi from '../../../utils/salvarNovoPostApi';

const NovoPost  =( {categorias} ) => {

    const [titulo, setTitulo] = useState('');
    const [categoria, setCategoria] = useState(-1);
    const [imagem, setImagem] = useState('');
    const [descricao,setDescricao] = useState('')
    const [autor, setAutor] = useState('')

    const [mensagem, setMensagem] = useState('')

const  incluirNovoPost =  evento => {

    //  console.log('Form \'submit\'.');

    evento.preventDefault();

    if ( !descricao || !titulo || !categoria || !imagem ) {
        alert('Por favor, preencha todos os campos!')

        return false;
    }

    const heute = new Date()

    const yare = heute.getFullYear()
    const monate = heute.getMonth()
    const tag = heute.getDate()

    const monateFormatado = (monate + 1) < 10 ?  "0" + String((monate + 1)) : String(monate +1)



    const  NovoPost  = {
          'titulo': titulo,
          'autor': autor,
          'categoriaId': categoria,
          'imagem': imagem, 
          'descricao': descricao, 
          'dataPostagem': `${yare}-${monateFormatado}-${tag}`
    
    }

    salvarNovoPostApi(NovoPost, setMensagem)
           
}    

return(
             
<div id='n-p'>   
         

      <h4 >Adicionar Novo Post:</h4>  

      {mensagem ? <p id='np-mensagem'>{mensagem}</p> : null}


       <form onSubmit={ evento => incluirNovoPost(evento)}>
          
          <div className="np-campo">
              <label htmlFor="np-campo-titulo">Título:</label>
              <input id="titulo" type="text" name="np-titulo" value={titulo} onChange={evento => setTitulo(evento.target.value)}
              placeholder="Adicione o Título..." />
          </div>

          <div className="np-campo">
              <label htmlFor="np-campo-Autor">Autor:</label>
              <input id="autor" type="text" name="np-autor" value={autor} onChange={evento => setAutor(evento.target.value)}
              placeholder="Adicione o nome do autor..." />
          </div>


          <div className="np-campo">
              <label htmlFor="np-campo-categoria">Categoria:</label>
              <select id="categoria"  name="np-categoria" value={categoria} onChange={evento => setCategoria(evento.target.value)} >
                 <option value={-1} disabled >Selecione uma Categoria...</option>
                 
                 {categorias.map(item => {
                  return    <option value={item.id} key={item.id} > {item.descricao} </option>
                 } )}
               
              
              </select>
          </div>


          <div className="np-campo">
              <label htmlFor="np-campo-imagem">Imagem Url:</label>
              <input id="imagem" type="text" name="np-imagem" value={imagem} onChange={evento => setImagem(evento.target.value)}
               placeholder="Adicione uma Url de imagem..."/>
          </div>


          <div className="np-campo">
              <label htmlFor="np-campo-texto">Texto:</label>
              <textarea id="texto-np" type="text" name="np-texto" value={descricao} onChange={evento => setDescricao(evento.target.value)}
              placeholder="Adicione um Texto..."/>
          </div>

          <input type="submit" value="Salvar" />



       </form>

    
</div>   

 

    )
}

export default NovoPost;


