import React from 'react';

import './Sobre.css';

const Sobre =() => {
    return(
     <div id='texto'> No ano de 2020 eu e minha esposa resolvemos conhecer a Austria
         e aprender a lingua Alemã, foi uma experiencia incrivel
         e vimos a dificuldade que é aprender essa lingua tão desafiadora 
         e interessante, por isso resolvemos criar esse blog para 
         ajudarmos a tirar algumas das duvidas mais comuns.
     <img className='img' src='/imagens/austria.jpg' alt=''/>
     </div>
    )
} 

export default Sobre;