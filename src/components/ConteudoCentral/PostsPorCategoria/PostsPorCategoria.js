
import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';

import './PostsPorCategoria.css';



// import pegarPosts from '../../../utils/pegarPosts';
import pegarPostsPeloIdCategoria from '../../../utils/pegarPostsPeloIdCategoria';
import pegarCategoriaPeloId from '../../../utils/pegarCategoriaPeloId';


const PostsPorCategoria =() => {

const {idCategoria} = useParams();   

// const [posts, setPosts] = useState([]);  
const [postsFiltrados, setPostsFiltrados] = useState([]);  
const [nomeCategoria, setNomeCategoria] = useState('');   

// useEffect( () => {
//       pegarPosts (setPosts);
// }, []);

// useEffect( () => {
//     if(posts.length > 0) {
//    const _postsFiltrados = posts.filter(p => p.idCategoria === parseInt (idCategoria) );
//     setPostsFiltrados(_postsFiltrados);
//     }
// }, [posts, idCategoria]);


useEffect ( async () => {
    const _posts = await pegarPostsPeloIdCategoria(idCategoria)
    setPostsFiltrados(_posts)
}, [idCategoria])

useEffect ( async () => {
    const categoria = await pegarCategoriaPeloId(idCategoria)
    setNomeCategoria(categoria.descricao)
}, [idCategoria])
  
    
    return(

        
     <div id='cc-detalhes-post' >

         <h3>Lista de posts por categoria - {nomeCategoria}</h3>

    
         <div>
             {postsFiltrados.map( post => {
                 return <div className='cc-detalhes-p'>{post.id} - {post.categoriaId} - {post.descricao}</div>
             })}
         </div>
       
     </div>
    )
} 

export default PostsPorCategoria;