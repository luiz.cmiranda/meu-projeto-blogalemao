import React, {useState} from 'react';

import './NovaCategoria.css';

import salvarNovaCategoriaServico from '../../../utils/salvarNovaCategoriaServico';

const NovaCategoria = ({categorias}) => {

  const [categoria, setCategoria] = useState('') 

  const salvarNovaCategoria = evento => {

    evento.preventDefault()

      if (!categoria) {
          alert('Preencha o nome da categoria')
          return false
      }
       
       const NovaCategoria = {
           "categoria": categoria
       }
    
      salvarNovaCategoriaServico(NovaCategoria)
  }

return(
             
      <div id="nc">
        
        <h5>Nova Categoria:</h5>

        <form onSubmit={ evento => salvarNovaCategoria(evento)} >

            <div className="nc-campo">
            <label> Nome da categoria:</label>
            <input id="nc-categoria" name="nc-categoria"
            onChange={evento => setCategoria(evento.target.value)} 
            placeholder="Escreva o nome da categoria..." />
            </div>

        <button id="botao-salvar">Salvar</button> 


        </form>

      </div>

    
    )
}

export default NovaCategoria;