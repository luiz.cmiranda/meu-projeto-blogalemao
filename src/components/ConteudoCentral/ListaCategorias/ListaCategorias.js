import React from 'react';
import { Link } from 'react-router-dom';

import './ListaCategorias.css';
import removerCategoriaServico from '../../../utils/removerCategoriaServico';


const ListaCategorias = ( {lista} ) => {

const removerCategoria = async (id) => {

    
    if( window.confirm('Você quer realmente apagar essa categoria?') ) {
    removerCategoriaServico(id)

    
    }

}

return <>
     <div id="lista-categorias"> 

        <div id='lc-titulo-btn-nova-categoria'>
            
            <h2 id='lc-titulo'>Lista Categorias:</h2>
            <Link to='nova-categoria' id='lc-btn-nova-categoria'>
            <button > Nova Categoria</button>
            </Link>
        </div>

        <ul id='lc-categoria'>
            {lista.map(item => <li className='lc-item' key={item.id}>{item.descricao}
             <button className='lc-item-btn-remover'
             onClick={ () => { removerCategoria(item.id) } } >
             Remover</button>
             <Link
              className='lc-item-bnt-posts'
              to={`posts-por-categoria/${ item.id}`}>
                Listar Posts
                </Link>
            </li>)}
        </ul>
     
        </div>
</>
} 

export default ListaCategorias;