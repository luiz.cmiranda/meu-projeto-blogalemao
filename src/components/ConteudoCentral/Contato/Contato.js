import React, {useState} from'react';

import './Contato.css';



const Contato =() => {


  const [email, setEmail] = useState('');
  const [nome, setNome] = useState('');
  const [mensagem, setMensagem] = useState('');
  const [anexo, setAnexo] = useState('');

const  incluirContato = evento => {

  //  console.log('Form \'submit\'.');

  evento.preventDefault();

  if ( !email || !nome || !mensagem || !anexo ) {
    alert('Por favor, preencha todos os campo!')

    return false;
}
    
}
    return(

      
      <form id='contato-lista' onSubmit={ evento => incluirContato(evento)}>
          <h6>Entre em contato com a gente:</h6>  
     <div className='Contato'> 

        
     
        <label htmlFor="email">E-mail:</label>
        <input type="text" id="email" name="email" placeholder="E-mail de destino.."
        value={email} onChange={evento => setEmail(evento.target.value)} />
 
        <label htmlFor="nome">Nome:</label>
        <input type="text" id="nome" name="nome" placeholder="Nome da pessoa.."
        value={nome} onChange={evento => setNome(evento.target.value)} />
 
        <label htmlFor="mensagem">Mensagem:</label>
        <textarea id="mensagem" name="mensagem" placeholder="Escreva algo.." 
        value={mensagem} onChange={evento => setMensagem(evento.target.value)} className="textArea"></textarea>
 
        <label htmlFor="anexo">Anexo:</label>
        <input type="file" id="anexo" name="anexo"
        value={anexo} onChange={evento => setAnexo(evento.target.value)} />
 
        <input type="submit"  value="Enviar" />
      

     </div>

     </form>
     )
}  

export default Contato;